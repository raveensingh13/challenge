import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: state,
  mutations: mutations
})

export const state = {
    list: []
};

export const getters = {
    datas: state => state.list
}

export const mutations = {
    saveList(state, payload) {
        state.list.push(payload);
    }
}